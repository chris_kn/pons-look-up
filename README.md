<h1>Pons LookUp</h1>
<h2>A simple, but useful Firefox Add-on</h2>
<p>
Adds a context menu item to search for a selected word in the PONS online dictionary. Clicking the menu item opens a new tab with the search results on pons.com. Languages can be changed on the Add-on's settings page.
</p>
<p>
Available on <a target="_blank" href="https://addons.mozilla.org/de/firefox/addon/ponslookup/">https://addons.mozilla.org/de/firefox/addon/ponslookup/</a>
</p>
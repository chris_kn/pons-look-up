const LanguageTerms = {
  de: {
    en: 'englisch',
    es: 'spanisch',
    fr: 'französisch',
    de: 'deutsch',
    it: 'italienisch',
    ar: 'arabisch',
    tr: 'türkisch',
    zh: 'chinesisch',
    la: 'latein',
    ru: 'russisch',
    el: 'griechisch'
  },
  en: {
    en: 'english',
    es: 'spanish',
    fr: 'french',
    de: 'german',
    it: 'italian',
    ar: 'arab',
    tr: 'turkish',
    zh: 'chinese',
    la: 'latin',
    ru: 'russian',
    el: 'greek'
  },
  fr: {
    en: 'anglais',
    es: 'espagnol',
    fr: 'francais',
    de: 'allemand',
    it: 'italien',
    ar: 'arabe',
    tr: 'turc',
    zh: 'chinois',
    la: 'latin',
    ru: 'russe',
    el: 'grec'
  },

}

export default LanguageTerms;
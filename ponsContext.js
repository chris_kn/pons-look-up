var translationTerm = {
  'en': 'translate',
  'es': 'traducción',
  'de': 'übersetzung',
  'fr': 'traduction',
  'it': 'traduzione',
  'tr': 'çeviri',
  'zh': '翻译'
};

var selLanguage;
var selDirection;
var selWord = "";

browser.contextMenus.create({
  id: "pons-selection",
  title: "Search On Pons.com",
  contexts: ["selection"]
}, onCreated);

function onCreated() {
  if (browser.runtime.lastError) {
    console.log(`Error: ${browser.runtime.lastError}`);
  } else {
    console.log("Pons Lookup context menu icon added.");
  }
}

function onGot(item) {
  console.log(item);
  if (item.languageSelect) {
    selLanguage = item.languageSelect;
  } else {
    selLanguage = "en";
  }
  if (item.langdirect) {
    selDirection = item.langdirect;
  } else {
    selDirection = "enes";
  }

  const urlString = `https://${selLanguage}.pons.com/${translationTerm[selLanguage]}?q=${selWord}&l=${selDirection}`;

  browser.tabs.create({
    active: true,
    url: urlString
  })
}

function onError(error) {
  console.log(`Error: ${error}`);
}

browser.contextMenus.onClicked.addListener(function (info, tab) {
  if (info.menuItemId == "pons-selection") {
    selWord = info.selectionText.trim();

    let gettingItem = browser.storage.local.get(["languageSelect", "langdirect"]);
    gettingItem.then(onGot, onError);
  }
});


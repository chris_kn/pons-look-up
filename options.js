function checkPageLanguage() {
    var radios = document.getElementsByName('pagelang');

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            return radios[i].value;
        }
    }
}

function setPageLanguage(selLang) {
    var radios = document.getElementsByName('pagelang');

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].value == selLang) {
            radios[i].checked = true;
        } else {
            radios[i].checked = false;
        }
    }
}

function checkTransLanguage() {
    var radios = document.getElementsByName('translang');

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            return radios[i].value;
        }
    }
}

function setTransLanguage(selLang) {
    var radios = document.getElementsByName('translang');

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].value == selLang) {
            radios[i].checked = true;
        } else {
            radios[i].checked = false;
        }
    }
}


function saveOptions(e) {
    if (e) {
        e.preventDefault();
    }
    
    browser.storage.local.set({
        languageSelect: checkPageLanguage(),
        langdirect: checkTransLanguage()
    });
}

function restoreOptions() {
    function setCurrentChoice(result) {
        if (result.languageSelect) {
            setPageLanguage(result.languageSelect);
        } else {
            setPageLanguage("en");
        }

        if (result.langdirect) {
            setTransLanguage(result.langdirect);
        } else {
            setTransLanguage('enes');
        }
    }

    function onError(error) {
        console.log(`Error: ${error}`);
    }

    var getting = browser.storage.local.get(["languageSelect", "langdirect"]);
    getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("change", saveOptions);